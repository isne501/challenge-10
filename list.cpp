#include <iostream>
#include "list.h"
using namespace std;
//deconstructor
List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

//headpush
void List::headPush(int el)
{
	Node *tmp = new Node(el); //declare new node
	if (head == NULL) //if not have any in list
	{
		head = tmp;
		tail = head;
	}
	else
	{
		//doubly link
		tmp->next = head; 
		head->prev = tmp;
		head = tmp;
		
	}
}

//tailpush
void List::tailPush(int el)
{
	Node *tmp = new Node(el);
	if (tail == NULL) //if not have any in list
	{
		tail = tmp;
		head = tail;
	}
	else
	{
		//doubly link
		tail->next = tmp;
		tmp->prev = tail;
		tail = tmp;
	}
}

int List::tailPop()
{
	Node *tmp = new Node(NULL); //declare new node
	int value; //value that store info tail
	if (tail->prev == 0) //if only one in the list
	{
		tmp = tail;
		value = tmp->info;
	}
	else {
		tmp = tail; 
		tail = tail->prev;
		value = tmp->info;//store value that now pointing
	}
	delete tmp; //delete element that pointing
	return value; // return value that pointing before deleted

}

//headpop
int List::headPop()
{
	Node *tmp = new Node(NULL); //declare new node
	int value; //value that store info tail
	if (head->next== 0) //if only one in the list
	{
		tmp = head;
		value = tmp->info;
	}
	else {
		tmp = head; 
		head = head->next;
		value = tmp->info; //store value that now pointing
	}
	delete tmp; //delete element that pointing
	return value; // return value that pointing before deleted
}

void List::deleteNode(int el)
{
	Node *tmp = head; //make new node point to head
	Node *tmp2 = head;

	if (head == NULL) 
	{
		head = head->next;
	}
	else
	{
		if (head == tail) //if element remain 1 do this
		{
			if (tmp->info == el)
			{
				head = NULL; //make head and tail point to NULL
				tail = NULL;//
				delete tmp; //delete element
			}
		}
		else
		{
			if (tail->info == el) //if element at tail equal to element that users need to delete
			{
				tmp2 = tail;
				tail = tail->prev; //doubly link
				tail->next = NULL;
				delete tmp2;
			}
			else
			{
				if (head->info == el)  //if element at head equal to element that users need to delete
				{
					head = head->next; //make head point next position
					delete tmp;
				}
				else
				{
					if (tmp->next->next != NULL) { //to delete element between of head and tail
						while (tmp->next->next != NULL)
						{
							if (tmp->next->info == el)
							{
								//doubly link skip the list that will be delete
								tmp2 = tmp->next;
								tmp->next = tmp2->next; 
								delete tmp2;
							}
							else
								tmp = tmp->next;
						}
					}
				}
			}
		}
	}
}
bool List::isInList(int el)
{
	Node *tmp = head;
	//codition that check element is in list True or false
	if (head == NULL || tmp == NULL)
	{
		return false;
	}
	else
	{
		while (tmp->next != NULL)
		{
			if (tmp->info == el)
				return true;
			else
				tmp = tmp->next; //to find anoter elements

		}
		return false;
	}
}

void List::showEl() //show elements
{
	Node *tmp = head;
	if (head == NULL)
		cout << "Empty!!." << endl;
	else {
		while (tmp != NULL)
		{
			cout << tmp->info << " ";
			tmp = tmp->next;
		}
	}
}